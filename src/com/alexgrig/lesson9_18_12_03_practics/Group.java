package com.alexgrig.lesson9_18_12_03_practics;

import java.util.Arrays;
import java.util.Objects;

public class Group {
    private String name;
    private Student[] students;

    public Group(String name) {
        this.name = name;
        students = new Student[10];
    }

    public String showStudents() {
        return Arrays.toString(students);
    }

    public Student[] getStudents() {
        return students;
    }

    public String getName() {
        return name;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(name, group.name) &&
                Arrays.equals(students, group.students);
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", students=" + Arrays.toString(students) +
                '}';
    }
}
