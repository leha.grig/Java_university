package com.alexgrig.lesson9_18_12_03_practics;

import java.util.Arrays;

public class University {
    private final String name;
    private Department[] departments;
    private Teacher[] teachers;

    public University(String name) {
        this.name = name;
        this.departments = new Department[10];
        this.teachers = new Teacher[10];
    }

    public String getName() {
        return name;
    }

    public Department[] getDepartments() {
        return departments;
    }

    public void setDepartments(Department[] departments) {
        this.departments = departments;
    }

    public Teacher[] getTeachers() {
        return teachers;
    }

    public void setTeachers(Teacher[] teachers) {
        this.teachers = teachers;
    }
    public void addDepartment (String name){
        boolean check = true;
        for (int i = 0; i < departments.length; i++) {
            if (departments[i].getName().equals(name)) {
                System.out.println("The department with name " + name + " is already exist");
                check = false;
            }
        }
        if (check) {
            for (int i = 0; i < departments.length; i++) {
                if (departments[i] == null) {
                    departments[i] = new Department(name);
                    break;
                } else {
                    Department[] temp = Arrays.copyOf(departments, departments.length * 2);
                    temp[departments.length] = new Department(name);
                    departments = temp;
                    break;
                }
            }
        }
    }

    public boolean deleteDepartment(Department department) {
        int count = 0;
        boolean check = false;
        for (int i = 0; i < departments.length; i++) {
            if (department.getName().equals(departments[i].getName())) {
                departments[i] = null;
                check = true;
            }
            if (departments[i] == null) {
                count++;
            }
        }
        if (check) {
            if (count > departments.length / 2) {
                Department[] temp = new Department[departments.length / 2];
                for (int i = 0, j = 0; i < departments.length; i++) {
                    if (departments[i] != null) {
                        temp[j] = departments[i];
                        j++;
                    }
                }
                departments = temp;
            }
            return true;
        }
        return false;
    }

    public void addTeacher (Teacher teacher){
        boolean check = true;
        for (int i = 0; i < teachers.length; i++) {
            if (teachers[i].equals(teacher)) {
                System.out.println("this teacher is already exist in University");
                check = false;
            }
        }
        if (check) {
            for (int i = 0; i < teachers.length; i++) {
                if (teachers[i] == null) {
                    teachers[i] = teacher;
                    break;
                } else {
                    Teacher [] temp = Arrays.copyOf(teachers, teachers.length * 2);
                    temp[teachers.length] = teacher;
                    teachers = temp;
                    break;
                }
            }
        }
    }

    public boolean deleteTeacher(Teacher teacher) {
        int count = 0;
        boolean check = false;
        for (int i = 0; i < teachers.length; i++) {
            if (teacher.equals(teachers[i])) {
                teachers[i] = null;
                check = true;
            }
            if (teachers[i] == null) {
                count++;
            }
        }
        if (check) {
            if (count > teachers.length / 2) {
                Teacher [] temp = new Teacher[teachers.length / 2];
                for (int i = 0, j = 0; i < teachers.length; i++) {
                    if (departments[i] != null) {
                        temp[j] = teachers[i];
                        j++;
                    }
                }
                teachers = temp;
            }
            return true;
        }
        return false;
    }
     public String showDepartments () {
        return Arrays.toString(departments);
     }
    public String showTeachers () {
        return Arrays.toString(teachers);
    }

    @Override
    public String toString() {
        return "University{" +
                "name='" + name + '\'' +
                ", departments=" + Arrays.toString(departments) +
                ", teachers=" + Arrays.toString(teachers) +
                '}';
    }
}
